module.exports = {
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    "primary-color": "#4CAF50"
                },
                javascriptEnabled: true,
            }
        }
    }
}
