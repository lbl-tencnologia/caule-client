import axios from "axios";
import getToken from "@/services/jwtService";

export const HTTP = axios.create({
  baseURL: `http://localhost:5000`,
  headers: {
    Authorization: "Bearer " + getToken
  }
});
