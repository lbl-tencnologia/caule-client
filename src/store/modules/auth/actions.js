import { HTTP } from "@/httpCommon";
import jwtService from "@/services/jwtService.js";

const authenticate = (context, credentials) => {
  let token = jwtService.getToken();
  if (!token) {
    HTTP.post("/auth", credentials)
      .then(function(data) {
        jwtService.saveToken(data.data.access_token);
      })
      .catch(function() {
        jwtService.destroyToken();
      });
  }
  context.commit("CAULE_TOKEN_UPDATE", token);
};

export default {
  authenticate
};
