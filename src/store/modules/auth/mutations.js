const CAULE_TOKEN_UPDATE = (state, cauleToken) => {
  state.cauleToken = cauleToken;
};

export default {
  CAULE_TOKEN_UPDATE
};
