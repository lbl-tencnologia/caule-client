const CAULE_TOKEN_UPDATE = (state, caule_token) => {
  state.caule_token = caule_token;
};

export default {
  CAULE_TOKEN_UPDATE
};
