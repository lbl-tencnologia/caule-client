import { HTTP } from "@/httpCommon";

const createUser = (_context, form) => {
  HTTP.post("/users/", form)
    .then(function(response) {
      console.log("then", response);
    })
    .catch(function(error) {
      console.log("error", error);
    });
};

export default {
  createUser
};
