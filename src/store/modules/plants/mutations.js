const PLANTS_UPDATED = (state, plants) => {
  state.plants = plants;
};

export default {
  PLANTS_UPDATED
};
