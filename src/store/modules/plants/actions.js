import { HTTP } from "@/httpCommon";
import jwtService from "@/services/jwtService.js";

const addPlant = (context, credentials) => {
  HTTP.post("/auth", credentials)
    .then(function(data) {
      jwtService.saveToken(data.data.access_token);
    })
    .catch(function() {
      jwtService.destroyToken();
    });
  context.commit("PLANTS_UPDATED", 5);
};

export default {
  addPlant
};
