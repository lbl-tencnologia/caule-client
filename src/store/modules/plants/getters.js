const allPlants = state => state.plants;

export default {
  allPlants
};
