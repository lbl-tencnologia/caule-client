const ID_TOKEN_KEY = "cauleToken";
const saveToken = token => {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};
const getToken = () => {
  return window.localStorage.getItem(ID_TOKEN_KEY);
};
const destroyToken = () => {
  window.localStorage.removeItem(ID_TOKEN_KEY);
};
export default {
  saveToken,
  getToken,
  destroyToken
};
