import Vue from "vue";
import Antd from "ant-design-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "ant-design-vue/dist/antd.less";
import BlankLayout from "./components/Layouts/BlankLayout.vue";
import LoggedLayout from "./components/Layouts/LoggedLayout.vue";

Vue.config.productionTip = false;
Vue.component("logged-layout", LoggedLayout);
Vue.component("blank-layout", BlankLayout);
Vue.use(Antd);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
