import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import SignIn from "./views/SignIn.vue";
import SignUp from "./views/SignUp.vue";
import Plants from "./views/Plants.vue";
import PlantDetails from "./views/PlantDetails.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "index",
      component: Home
    },
    {
      path: "/home",
      name: "home",
      component: Home
    },
    {
      path: "/sign_in",
      name: "signIn",
      component: SignIn
    },
    {
      path: "/sign_up",
      name: "signUp",
      component: SignUp
    },
    {
      path: "/plants",
      name: "plants",
      meta: { layout: "logged-layout" },
      component: Plants
    },
    {
      path: "/plantsDetails",
      name: "plantsDetails",
      meta: { layout: "logged-layout" },
      component: PlantDetails
    }
  ]
});
